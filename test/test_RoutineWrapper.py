# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

from qat.lang.AQASM.gates import X
from qat.lang.AQASM.classarith import cuccaro_add

from qprof import RoutineWrapper
from test.utils import (
    noparam_empty_gate,
    param_empty_gate,
    simple_x_gate,
    everywhere_x_gate,
    nested_gate,
    tolink_gate,
    boxed_empty_gate,
    boxed_multi_x_gate,
    named_boxed_gate,
    multi_controlled_gate,
    controlled_routine,
    rotation_gate,
)


def test_empty_creation():
    RoutineWrapper(noparam_empty_gate())
    RoutineWrapper(param_empty_gate(1, 3.14))


def test_creation_simple_routine():
    RoutineWrapper(simple_x_gate(2))


def test_creation_nested_routine():
    RoutineWrapper(nested_gate(2))


def test_creation_control():
    RoutineWrapper(noparam_empty_gate(), _number_of_controlled_qubits=2)


def test_creation_linking_set():
    RoutineWrapper(noparam_empty_gate(), linking_set=[param_empty_gate(3, 4.0)])


def test_creation_link_gate():
    RoutineWrapper(tolink_gate(9), linking_set=[nested_gate])


def test_creation_empty_boxed_gate():
    RoutineWrapper(boxed_empty_gate())


def test_creation_multi_x_boxed_gate():
    RoutineWrapper(boxed_multi_x_gate(10))


def test_creation_named_boxed_gate():
    RoutineWrapper(named_boxed_gate())


def test_creation_multi_controlled():
    RoutineWrapper(multi_controlled_gate(3))


def test_creation_multi_controlled_routine():
    RoutineWrapper(controlled_routine())


def test_iterate_empty():
    empty = RoutineWrapper(noparam_empty_gate())
    empty_iter = list(empty)
    assert len(empty_iter) == 0


def test_iterate_simple_x():
    simple_x = RoutineWrapper(simple_x_gate(9))
    assert not simple_x.is_base
    simple_x_iter = list(simple_x)
    assert len(simple_x_iter) == 1
    assert id(simple_x_iter[0]._gate) == id(X)


def test_iterate_multi_x():
    multi_x = RoutineWrapper(everywhere_x_gate(9))
    assert not multi_x.is_base
    multi_x_iter = list(multi_x)
    assert len(multi_x_iter) == 9
    assert all(id(g._gate) == id(X) for g in multi_x_iter)


def test_iterate_nested():
    nested = RoutineWrapper(nested_gate(9))
    assert not nested.is_base
    first_level_iter = list(nested)
    second_level_iter = list(first_level_iter[0])
    assert len(first_level_iter) == 1
    assert len(second_level_iter) == 9
    assert all(id(g._gate) == id(X) for g in second_level_iter)


def test_iterate_linked_gate():
    linked = RoutineWrapper(tolink_gate(9), linking_set=[nested_gate])
    assert not linked.is_base
    first_level_iter = list(linked)
    nested_level_iter = list(first_level_iter[0])
    multi_x_level_iter = list(nested_level_iter[0])
    assert len(first_level_iter) == 1
    assert len(nested_level_iter) == 1
    assert len(multi_x_level_iter) == 9
    assert all(id(g._gate) == id(X) for g in multi_x_level_iter)


def test_iterate_boxed_gate():
    boxed = RoutineWrapper(boxed_multi_x_gate(10))
    assert not boxed.is_base
    boxed_iter = list(boxed)
    assert len(boxed_iter) == 10
    assert all(id(g._gate) == id(X) for g in boxed_iter)


def test_iterate_multi_controlled():
    multi_control = RoutineWrapper(multi_controlled_gate(3))
    assert not multi_control.is_base
    multi_control_iter = list(multi_control)
    assert len(multi_control_iter) == 1


def test_iterate_multi_controlled_routine():
    multi_controlled_routine = RoutineWrapper(controlled_routine())
    assert not multi_controlled_routine.is_base
    first_level_iter = list(multi_controlled_routine)
    assert len(first_level_iter) == 1
    assert first_level_iter[0].is_controlled
    second_level_item = list(first_level_iter[0])
    assert len(second_level_item) == 1
    assert second_level_item[0].is_controlled


def test_is_base():
    simple_x = RoutineWrapper(simple_x_gate(9))
    assert not simple_x.is_base
    assert list(simple_x)[0].is_base


def test_is_base_linked_gate():
    linked = RoutineWrapper(tolink_gate(9), linking_set=[nested_gate])
    first_level_iter = list(linked)
    nested_level_iter = list(first_level_iter[0])
    multi_x_level_iter = list(nested_level_iter[0])
    assert len(first_level_iter) == 1
    assert not first_level_iter[0].is_base
    assert len(nested_level_iter) == 1
    assert not nested_level_iter[0].is_base
    assert len(multi_x_level_iter) == 9
    assert all(g.is_base for g in multi_x_level_iter)


def test_is_base_rotation():
    rotation = RoutineWrapper(rotation_gate())
    rotation_iter = list(rotation)
    assert not rotation.is_base
    assert len(rotation_iter) == 1
    assert rotation_iter[0].is_base


def test_name_simple_x():
    simple_x = RoutineWrapper(simple_x_gate(9))
    simple_x_iter = list(simple_x)
    assert simple_x.name == "simpleX"
    assert len(simple_x_iter) == 1
    assert simple_x_iter[0].name == "X"


def test_name_multi_controlled():
    routine = RoutineWrapper(multi_controlled_gate(3))
    multi_control_iter = list(routine)
    assert routine.name == "multi_controlled"
    assert len(multi_control_iter) == 1
    assert multi_control_iter[0].name == "CCCNOT"


def test_hash():
    routines = [
        RoutineWrapper(simple_x_gate(9)),
        RoutineWrapper(rotation_gate()),
        RoutineWrapper(multi_controlled_gate(3)),
        RoutineWrapper(cuccaro_add(10, 10)),
    ]
    for i in range(len(routines)):
        assert hash(routines[i]) == hash(routines[i])
        for j in range(i + 1, len(routines)):
            assert hash(routines[i]) != hash(routines[j])


def test_eq():
    routines = [
        RoutineWrapper(simple_x_gate(9)),
        RoutineWrapper(rotation_gate()),
        RoutineWrapper(multi_controlled_gate(3)),
        RoutineWrapper(cuccaro_add(10, 10)),
    ]
    for i in range(len(routines)):
        assert routines[i] == routines[i]
        for j in range(i + 1, len(routines)):
            assert not routines[i] == routines[j]


def test_is_controlled():
    multi_controlled = RoutineWrapper(multi_controlled_gate(3))
    multi_iter = list(multi_controlled)
    assert multi_iter[0].is_controlled


def test_creation_cuccaro_add():
    RoutineWrapper(cuccaro_add(10, 10))
