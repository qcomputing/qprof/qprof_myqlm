# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

from qat.lang.AQASM.gates import X
from qat.lang.AQASM.program import Program
from qat.lang.AQASM.routines import QRoutine

from qprof import supports_routine
from test.utils import noparam_empty_gate, param_empty_gate


def test_support_routine():
    assert not supports_routine(QRoutine())
    assert supports_routine(noparam_empty_gate())
    assert supports_routine(param_empty_gate(3, 2.248))
    assert not supports_routine(Program())
    assert not supports_routine(X)
