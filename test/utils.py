# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

from qat.lang.AQASM.gates import X, RX, AbstractGate
from qat.lang.AQASM.misc import build_gate
from qat.lang.AQASM.routines import QRoutine


@build_gate("noparam", [], arity=3)
def noparam_empty_gate():
    return QRoutine(arity=3)


@build_gate("param", [int, float], arity=3)
def param_empty_gate(n: int, f: float):
    return QRoutine(arity=3)


@build_gate("simpleX", [int], arity=lambda x: x)
def simple_x_gate(n: int):
    rout = QRoutine(arity=n)
    rout.apply(X, n // 2)
    return rout


@build_gate("multiX", [int], arity=lambda x: x)
def everywhere_x_gate(n: int):
    rout = QRoutine(arity=n)
    for i in range(n):
        rout.apply(X, i)
    return rout


@build_gate("nested", [int], arity=lambda n: n)
def nested_gate(n: int):
    rout = QRoutine(arity=n)
    rout.apply(everywhere_x_gate(n), list(range(n)))
    return rout


@build_gate("tolink", [int], arity=lambda n: n)
def tolink_gate(n: int):
    rout = QRoutine(arity=n)
    rout.apply(AbstractGate("nested", [int], arity=lambda x: x)(n), list(range(n)))
    return rout


def boxed_empty_gate():
    rout = QRoutine(arity=1)
    return rout.box()


def boxed_multi_x_gate(n: int):
    rout = QRoutine(arity=n)
    for i in range(n):
        rout.apply(X, i)
    return rout.box()


def named_boxed_gate():
    return QRoutine(arity=1).box("boxed_routine")


@build_gate("multi_controlled", [int], arity=lambda n: n + 1)
def multi_controlled_gate(n: int):
    rout = QRoutine(arity=n + 1)
    gate = X
    for _ in range(n):
        gate = gate.ctrl()
    rout.apply(gate, list(range(n + 1)))
    return rout


@build_gate("rotation_gate", [], arity=1)
def rotation_gate():
    rout = QRoutine(arity=1)
    rout.apply(RX(2.0), 0)
    return rout


@build_gate("controlled_routine", [], arity=6)
def controlled_routine():
    rout = QRoutine(arity=6)
    rout.apply(multi_controlled_gate(4).ctrl(), list(range(6)))
    return rout
