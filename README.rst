**qprof_interfaces**
====================

**qprof_qat** is a plugin for `qprof <https://gitlab.com/qcomputing/qprof/qprof>`_, a quantum profiler largely inspired by
`gprof <https://ftp.gnu.org/old-gnu/Manuals/gprof-2.9.1/html_chapter/gprof_1.html#SEC1>`_.
This plugin adds the support for the ``qat`` library to
`qprof <https://gitlab.com/qcomputing/qprof/qprof>`_.
It is a Python module distributed under the open-source (non-OSI but BSD compatible)
`CeCILL-B licence <https://cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_.

Installation
------------

*qprof_qat* being a Python module, it is installable with ``pip``.

From Gitlab
~~~~~~~~~~~

.. code:: shell

   git clone https://gitlab.com/cerfacs/qprof_interfaces.git
   pip install qprof_qat/

From PyPi
~~~~~~~~~

The code has not been published on PyPi yet.

